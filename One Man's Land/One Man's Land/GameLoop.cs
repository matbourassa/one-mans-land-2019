﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace One_Man_s_Land
{
    class GameLoop
    {
        GameCreator gameCreator;   //the game loop access the game through the gameCreator object passed by the Main

        public GameLoop(GameCreator gameCreator)
        {
            this.gameCreator = gameCreator;
        }
        
        public void Initialize(GraphicsDevice graphicsDevice)   //initializing objects for a giving level
        {
            switch (GameState.state)
            {
                case GameState.State.leveltest1:
                    gameCreator.levelTest1.Initialize(graphicsDevice);
                    break;
                case GameState.State.leveltest2:
                    gameCreator.levelTest2.Initialize(graphicsDevice);
                    break;
            }
        }

        public void Load(ContentManager content)  //Loading game assets for a giving level
        {
            switch (GameState.state)
            {
                case GameState.State.leveltest1:
                    gameCreator.levelTest1.Load(content);
                    break;
                case GameState.State.leveltest2:
                    gameCreator.levelTest2.Load(content);
                    break;
            }
        }

        public void Update(GameTime gameTime)  //the first part of the core loop - the game logic is resolved here
        {
            switch (GameState.state)
            {
                case GameState.State.leveltest1:
                    gameCreator.levelTest1.Update(gameTime);
                    break;
                case GameState.State.leveltest2:
                    gameCreator.levelTest2.Update(gameTime);
                    break;
            }
        }

        public void Draw(SpriteBatch spriteBatch)   //the second part of the core loop - the game is drawn here
        {
            switch (GameState.state)
            {
                case GameState.State.leveltest1:
                    gameCreator.levelTest1.Draw(spriteBatch);
                    break;
                case GameState.State.leveltest2:
                    gameCreator.levelTest2.Draw(spriteBatch);
                    break;
            }
        }
    }
}
