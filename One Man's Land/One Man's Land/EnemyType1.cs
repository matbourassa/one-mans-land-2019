﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;


namespace One_Man_s_Land
{
    class EnemyType1 : ICollisionable
    {
        public Vector2 enemyPosition;
        Vector2 oldEnemyPosition;
        Texture2D enemyTexture;
        Texture2D enemyTexture2;
        public Vector2 velocity;
        Rectangle enemyRectangle;
        float AnchorPoint;
        bool awarness;
        Player player;
        BoundingSphere awarnessSphere;
        BoundingSphere positioningSphere;
        
        public BulletManager bulletManager;
        bool TriggerPulled;
        bool NextBulletReady;

        const int EnemyWidth = 192;
        const int EnemyHeight = 256;
        Vector2 TextureDimension = new Vector2(192, 256);

        int timeToReshoot = 50;  
        int ammunition = 4;
        int timeToReload = 300; 
        float shootingTime = 34.5f;

        bool Sound = true;
        bool Alive = true;
        
                       
        AnimationHelper enemyAnimationHelper = new AnimationHelper(EnemyWidth, EnemyHeight);

        public AnimationState enemyAnimationState = new AnimationState(); //enum de l'état de l'animation
        public EnergyState enemyEnergyState = new EnergyState();
        Type enemyType;

        CollisionManager collisionManager;

        CollisionGridLevelTest collisionGridLevelTest;
        

        public EnemyType1(Player player, CollisionManager collisionManager)
        {
            this.player = player;
            this.collisionManager = collisionManager;
        }

        public void Initialize(GraphicsDevice graphicsDevice)
        {
            enemyPosition = new Vector2(1600, 700);
            AnchorPoint = enemyPosition.X;
            awarness = false;
            enemyAnimationState = AnimationState.isPatrolling;
            velocity.X = 3;
            TriggerPulled = false;
            NextBulletReady = false;
            bulletManager = new BulletManager(collisionManager);
            collisionGridLevelTest = new CollisionGridLevelTest();
            enemyType = new Type();
            enemyType = Type.EnemyType1;
                        
        }
                
        public void Load(ContentManager content)
        {
            enemyTexture = content.Load<Texture2D>("EnemyType1/EnemyType1");
            enemyTexture2 = content.Load<Texture2D>("EnemyType1/Enemy01SpreadSheet");
            enemyAnimationHelper.Load(enemyTexture2);
            bulletManager.Load(content);
            
        }


        public void Update(GameTime gameTime, Texture2D LevelTexture)
        {
            enemyRectangle = new Rectangle((int)enemyPosition.X, (int)enemyPosition.Y, enemyTexture.Height, enemyTexture.Width);
            enemyPosition += velocity;
            awarnessSphere = new BoundingSphere(new Vector3(enemyPosition.X + (EnemyWidth / 2), enemyPosition.Y + (EnemyHeight / 2), 0), 500);
            positioningSphere = new BoundingSphere(new Vector3(enemyPosition.X + (EnemyWidth / 2), enemyPosition.Y + (EnemyHeight / 2), 0), 450);

           
            FloorCollisionDetection(LevelTexture);
            StateModifier();
            StateReaction(gameTime);
            bulletManager.Update(); 


            if (velocity.Y < 10)
                velocity.Y += 10;

            oldEnemyPosition = enemyPosition;
        }
        
        public void Draw(SpriteBatch spriteBatch)
        {

            if (Alive)
            {
                if (enemyAnimationState == AnimationState.isPatrolling)
                    spriteBatch.Draw(enemyTexture, enemyPosition, Color.White);

                if (enemyAnimationState == AnimationState.isReadyToShoot)
                    spriteBatch.Draw(enemyTexture, enemyPosition, Color.Red);
                
                if (enemyAnimationState == AnimationState.hasStartShootingLeft)
                    enemyAnimationHelper.Draw(spriteBatch, enemyPosition);

                if (enemyAnimationState == AnimationState.isChasing)
                    spriteBatch.Draw(enemyTexture, enemyPosition, Color.Blue);

                if (enemyAnimationState == AnimationState.isReloading)
                    spriteBatch.Draw(enemyTexture, enemyPosition, Color.Purple);

                if (enemyAnimationState == AnimationState.isPositioning)
                    spriteBatch.Draw(enemyTexture, enemyPosition, Color.Green);
            }
            bulletManager.Draw(spriteBatch);
            
        }


        private void FloorCollisionDetection(Texture2D LevelTexture)
        {
            //Détection de collision 
            collisionGridLevelTest.VerifyHorizontalCollision(enemyPosition, LevelTexture, enemyRectangle, velocity.X);
            
            //Ajustement de la vélocité, et de la position verticale et horizontale suite à la détection de collision 
            enemyPosition.Y = collisionGridLevelTest.AdjustHorizontalePosition();
            enemyPosition.X = collisionGridLevelTest.AdjustVerticalePosition();
            velocity.X = collisionGridLevelTest.AdjustVerticaleVelocity();
        }

        
        public void StateModifier()
        {
            if (!awarness)
            {
                if (awarnessSphere.Intersects(player.positionSphere))
                { enemyAnimationState = AnimationState.isReadyToShoot; awarness = true; }
                else
                { enemyAnimationState = AnimationState.isPatrolling; }
            }

            if (awarness)
            {
                if (positioningSphere.Intersects(player.positionSphere))
                { enemyAnimationState = AnimationState.isPositioning; }
                else if (awarnessSphere.Intersects(player.positionSphere))
                { enemyAnimationState = AnimationState.isReadyToShoot; }
                else
                { enemyAnimationState = AnimationState.isChasing; }
            }

            if (ammunition == 0)
            { enemyAnimationState = AnimationState.isReloading; }

            

        }
        
        public void StateReaction(GameTime gameTime)
        {
            if (enemyAnimationState == AnimationState.isStandingRight)
            {
                velocity.X = 0;
            }

            if (enemyAnimationState == AnimationState.isPatrolling)
            {

                if (enemyPosition.X < AnchorPoint - 300)
                    velocity.X = 3;
                if (enemyPosition.X > AnchorPoint + 300)
                    velocity.X = -3;
            }

            if (enemyAnimationState == AnimationState.isReadyToShoot)
            {
                
                velocity.X = 0;
                timeToReshoot--;
                
                if (timeToReshoot < 0)
                { enemyAnimationState = AnimationState.hasStartShootingLeft; }
                
            }

            if (enemyAnimationState == AnimationState.hasStartShootingLeft)
            {
                if (Sound)
                { SoundFx.EnemyType01GunShotEffect(); Sound = false; }

                TriggerPulled = true;
                if (shootingTime >= 34) //Pour ne pas tirer une nouvelle balle tant que l'animation n'est pas terminée, le shootingTime -durée de l'animation-  est de 34.5f
                    NextBulletReady = true;
                else NextBulletReady = false;

                bulletManager.ShootBullet(player.PlayerPosition.X, player.PlayerPosition.Y + 100, enemyPosition.X, enemyPosition.Y + 45, TriggerPulled, NextBulletReady);
                
                enemyAnimationHelper.Update(gameTime, 80, 0, 7, false);
                shootingTime--;
                if (shootingTime < 0)
                { enemyAnimationState = AnimationState.isDoneShootingLeft; } 

            }


            if (enemyAnimationState == AnimationState.isDoneShootingLeft)
            {
                Sound = true;
                timeToReshoot = 50; 
                ammunition--;
                shootingTime = 34.5f;
            }

            if (enemyAnimationState == AnimationState.isReloading)
            {
                velocity.X = 0;
                timeToReload--;
                if (timeToReload == 0)
                { enemyAnimationState = AnimationState.isReadyToShoot; timeToReload = 300; ammunition = 4; } 
            }

            if (enemyAnimationState == AnimationState.isChasing)
            {
                if (enemyPosition.X < player.PlayerPosition.X)
                    velocity.X = 5;
                if (enemyPosition.X > player.PlayerPosition.X)
                    velocity.X = -5;
            }

            if (enemyAnimationState == AnimationState.isPositioning)
            {
                if (enemyPosition.X < player.PlayerPosition.X)
                    velocity.X = -5;
                if (enemyPosition.X > player.PlayerPosition.X)
                    velocity.X = 5;
                            
            }


            if (enemyEnergyState == EnergyState.isDyingRight)
            { }

            if (enemyEnergyState == EnergyState.isDead)
            { }
        }

        public Vector2 GetPosition()
        {
            return enemyPosition;
        }

        public Rectangle GetCollisionRectangle()
        {
            return enemyRectangle;
        }

        public new Type GetType()
        {
            return enemyType;
        }

        public AnimationState GetState()
        {
            return enemyAnimationState;
        }

        public void ReactionToCollision(Type type, Vector2 Position, AnimationState playerState)
        {
            if (type == Type.Player)
            {
                if (playerState == AnimationState.isHighPunchRight)
                { Alive = false; }

            }

        }



    }
}
