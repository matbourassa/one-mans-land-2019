﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace One_Man_s_Land
{
    class HealthBar
    {

        Vector2 BarPosition;
        Texture2D BarTexture;
        Vector2 UnderLayerBarPosition;
        Texture2D UnderLayerBarTexture;
        Rectangle ZoneToDraw;
        int ZoneToDrawWidht;
        int VariableBarPosition;
        int OldZoneToDrawWidth;
        int speed = 2;


        public void Initialize()
        {
            UnderLayerBarPosition = new Vector2(460, 1040);
            BarPosition = new Vector2(460, 1040);
            
        }


        public void Load(ContentManager content)
        {
            BarTexture = content.Load<Texture2D>("HealthBar/HealthBar");
            UnderLayerBarTexture = content.Load<Texture2D>("HealthBar/HealthBarUnderLayer");
            VariableBarPosition = 460;
            BarPosition.X = VariableBarPosition;
            ZoneToDrawWidht = 1000;
        }


        public void Update()
        {

            ZoneToDraw = new Rectangle(VariableBarPosition, (int)BarPosition.Y, ZoneToDrawWidht, BarTexture.Height);

            if (ZoneToDrawWidht > 200)
            {
                ZoneToDrawWidht = ZoneToDrawWidht - speed ;
                VariableBarPosition++;
            }


            if (OldZoneToDrawWidth < 200)
            {
                ZoneToDrawWidht = ZoneToDrawWidht + speed;
                VariableBarPosition--;
            }

            OldZoneToDrawWidth = ZoneToDrawWidht;
            BarPosition.X = VariableBarPosition;


        }


        public void Draw(SpriteBatch spriteBatch)
        {

            spriteBatch.Draw(UnderLayerBarTexture, UnderLayerBarPosition, Color.White);
            spriteBatch.Draw(BarTexture, BarPosition, ZoneToDraw, Color.White);
            
        }

        
    }
}
