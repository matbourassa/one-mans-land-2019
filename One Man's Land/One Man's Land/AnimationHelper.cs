﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;


namespace One_Man_s_Land
{
    class AnimationHelper
    {

        Rectangle SourceFrameToDraw; //Le frame a dessiner à chaque cycle
        Texture2D SpriteSheetTexture; //Le ficher à utiliser
        int CurrentFrameX; //La position horizontale sur le spreadSheet
        int CurrentRow; //Quelle rangée à utiliser sur l'axe des Y - donc quelle animation faire
        int FrameWidth; //Largeur du frame à dessiner
        int FrameHeight; //Hauteur du frame à dessiner
        float AnimationTimer; //Délai en milliseconde entre les frames
        bool animationDone = false;

        //Le constructeur a besoin d'avoir la dimension d'un frame 
        public AnimationHelper(int FrameWidth, int FrameHeight)
        {
            this.FrameWidth = FrameWidth;
            this.FrameHeight = FrameHeight;
            CurrentFrameX = 0;
        }

        //Le spreadSheet à utiliser
        public void Load(Texture2D SpriteSheetTexture)
        {
            this.SpriteSheetTexture = SpriteSheetTexture;
        }

        //La méthode Update a besoin de connaitre l'état de l'animation, la vitesse de l'animation <AnimationInterval>, la rangé Y sur le spreadsheet <CurrentRow>, 
        //et le nombre de frames que contient l'animation <TotalFrameX> 
        public void Update(GameTime gameTime, float AnimationInterval, int _currentRow, int TotalFrameX, bool Jump) //Le bool donne acces a la zone de ''sustain'' du dernier frame (pour l<animation du saut, entre autres choses
        {
            CurrentRow = _currentRow;
            AnimationTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (AnimationTimer > AnimationInterval)
            {
                CurrentFrameX++;
                AnimationTimer = 0;
            }

            if (!Jump)
            {
                if (CurrentFrameX > TotalFrameX - 1)
                {
                    animationDone = true;
                    CurrentFrameX = 0;
                }
            }

            if (Jump)
            {
                if (CurrentFrameX >= TotalFrameX)
                {
                    CurrentFrameX = TotalFrameX - 1;
                }
            }

        }

        public bool GetAnimationIsDone()
        {
            return animationDone;
        }

        public void SetAnimationIsdone(bool animationDone)
        {
            this.animationDone = animationDone;

        }

        public void SetCurrentFrame(int CurrentFrameX)
        {
            this.CurrentFrameX = CurrentFrameX;
        }


        //La méthode Draw créee le rectangle sur le spreadSheet <SourceFrameToDraw>, et dessine le sprite
        //à la position passée en paramètre
        public void Draw(SpriteBatch spriteBatch, Vector2 SpritePosition)
        {
            SourceFrameToDraw = new Rectangle(CurrentFrameX * FrameWidth, CurrentRow * FrameHeight, FrameWidth, FrameHeight);
            spriteBatch.Draw(SpriteSheetTexture, SpritePosition, SourceFrameToDraw, Color.White);
        }


    }
}



