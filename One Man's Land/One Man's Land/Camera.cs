using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;


namespace One_Man_s_Land
{
    class Camera
    {
        Matrix transform;
        public Matrix Transform
        {
            get { return transform; }
        }

        public Vector2 center;
        private Viewport viewport;
        public Player player;






        public Camera(Viewport newViewport, Player newPlayer)
        {
            viewport = newViewport;
            player = newPlayer;
        }

        public void Update(Vector2 playerposition, int xOffset, int yOffset, float parallaxeFactor)
        {


            if (playerposition.X < viewport.Width / 2)
                center.X = viewport.Width / 2;
            else if (playerposition.X > xOffset - (viewport.Width / 2))
                center.X = xOffset - (viewport.Width / 2);
            else center.X = playerposition.X;


            if (playerposition.Y < viewport.Height / 2)
                center.Y = viewport.Height / 2;
            else if (playerposition.Y > yOffset - (viewport.Height / 2))
                center.Y = yOffset - (viewport.Height / 2);
            else center.Y = playerposition.Y;


            transform = Matrix.CreateTranslation(new Vector3(-center.X + (viewport.Width / 2), -center.Y + (viewport.Height / 2), 0) * parallaxeFactor);




        }
    }

}


      
