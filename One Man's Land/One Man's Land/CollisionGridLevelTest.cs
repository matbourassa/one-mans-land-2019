﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace One_Man_s_Land
{
    class CollisionGridLevelTest
    {
        //Variable temporaire qui conserve la position corrigé du sprite qui la demande
        Vector2 Position;
        float VelocityX;
        bool onTheFloor;

        //Sert d'accès pour corriger la position horizontale du sprite demandé - la méthode LineCollisionVerification() modifie la donnée membre <Position>
        public float AdjustHorizontalePosition()
        {
            return Position.Y;
        }

        //Sert d'accès pour corriger la position verticale du sprite demandé - la méthode VerticalCollision() modifie la donnée membre <Position>
        public float AdjustVerticalePosition()
        {
            return Position.X;
        }

        //Sert d'accès pour corriger la velocity verticale du sprite demandé - la méthode VerticalCollision() modifie la donnée membre <VelocityX>
        public float AdjustVerticaleVelocity()
        {
            return VelocityX;
        }

        //Savoir si le player est sur le sol ou non
        public bool OnTheFloor()
        {
            return onTheFloor;
        }

        //Modification de onTheFloor (voir methode precedente)
        public void SetOnTheFloor(bool onTheFloor)
        {
            this.onTheFloor = onTheFloor;

        }
        
        //Construit la liste des points constituant la ligne brisée formant le plancher + gère la liste des obstacles verticaux
        public void VerifyHorizontalCollision(Vector2 tempPosition, Texture2D LevelTexture, Rectangle SpriteRectangle, float tempVelocity)
        {
            Position = tempPosition;

            var CoordonneList = new List<Tuple<Vector2, Vector2>>
            {
                new Tuple<Vector2, Vector2>(new Vector2(0, 1200), new Vector2(200, 1175)),
                new Tuple<Vector2, Vector2>(new Vector2(200, 1175), new Vector2(600, 1150)),
                new Tuple<Vector2, Vector2>(new Vector2(600, 1150), new Vector2(1500, 1000)),
                new Tuple<Vector2, Vector2>(new Vector2(1500, 1000), new Vector2(2000, 890)),
                
                new Tuple<Vector2, Vector2>(new Vector2(1900, 1110), new Vector2(2020, 1140)),
                new Tuple<Vector2, Vector2>(new Vector2(2020, 1140), new Vector2(2900, 1110)),
                new Tuple<Vector2, Vector2>(new Vector2(2900, 1110), new Vector2(3400, 1140)),
                new Tuple<Vector2, Vector2>(new Vector2(3400, 1140), new Vector2(3790, 1170)),
                new Tuple<Vector2, Vector2>(new Vector2(3790, 1170), new Vector2(4000, 1140)),

            };

            Rectangle DecorRectangle1 = new Rectangle(1800, 990, 100, 300);

            foreach (Tuple<Vector2, Vector2> Coordonne in CoordonneList)
            {
                Vector2 Start;
                Vector2 End;

                Start = Coordonne.Item1;
                End = Coordonne.Item2;

                CollisionVerification(Start, End, tempPosition, SpriteRectangle, LevelTexture, tempVelocity, DecorRectangle1);
            }
        }

        //Calcule si le sprite dépasse la ligne du plancher et corrige la position au besoin.  
        //La variable Position de cette classe conserve la position corrigée.
        private void CollisionVerification(Vector2 StartSeg, Vector2 EndSeg,  Vector2 tempPosition, Rectangle SpriteRectangle, Texture2D LevelTexture, float tempVelocity, Rectangle DecorRectangle1)
        {
            VelocityX = tempVelocity;

            if (SpriteRectangle.Intersects(DecorRectangle1))
            {
                Position.X += 0.55f;  
                VelocityX = 0;
            }

            if ((tempPosition.X + 100 > StartSeg.X) && ((tempPosition.X + 60) < EndSeg.X))
            {
                //Y = mx + c
                float m = (EndSeg.Y - StartSeg.Y) / (EndSeg.X - StartSeg.X); // Pente
                float c = StartSeg.Y - (m * StartSeg.X); // Y Intercept

                //Résolution de l'équation
                if (((tempPosition.Y + 256) > (m * tempPosition.X + c)) && ((tempPosition.Y + 256) < (m * tempPosition.X + c) + 148)) //Une mince bande sous le rail dans laquelle la collision se résoud.  Comme ça, on peut passer sous une ligne.
                {
                    Position.Y = ((m * tempPosition.X + c) - 240);
                    onTheFloor = true;
                }
            }

            //Limite du monde gauche et droite
            if (tempPosition.X < 0) Position.X = 0;
            if (tempPosition.X > LevelTexture.Width - (SpriteRectangle.Width * 4)) Position.X = LevelTexture.Width - (SpriteRectangle.Width * 4);
           
        }

    }
}
