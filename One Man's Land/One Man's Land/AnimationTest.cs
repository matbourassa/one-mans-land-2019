﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace One_Man_s_Land
{
    class AnimationTest
    {
        
        Texture2D PlayerSpreadSheet;
        Vector2 PlayerPosition;
        Rectangle PlayerRectangle;
        Rectangle SourceFrame;
        int CurrentFrameX;
        int CurrentFrameY;
        float Interval;
        const int PlayerWidth = 192;
        const int PlayerHeight = 256;
        float Timer;

        enum PlayerAnimationState{StandingRight, StandingLeft, WalkingRight, WalkingLeft, RunningRight, RunningLeft, JumpingRight, JumpingLeft};
        PlayerAnimationState playerAnimationState = new PlayerAnimationState();



        public void Initialize()
        {
            PlayerPosition = new Vector2(800, 500);
            playerAnimationState = PlayerAnimationState.StandingRight;
            CurrentFrameX = 0;
            CurrentFrameY = 4;
            Interval = 100f;
        }


        public void Load(ContentManager content)
        { 
            PlayerSpreadSheet = content.Load<Texture2D>("Animation Test/PlayerSpreadSheet");
        }


        public void Update(GameTime gameTime)
        {
            Timer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            SourceFrame = new Rectangle(CurrentFrameX * PlayerWidth, CurrentFrameY * PlayerHeight, PlayerWidth, PlayerHeight);
            PlayerRectangle = new Rectangle((int)PlayerPosition.X, (int)PlayerPosition.Y, PlayerWidth, PlayerHeight);



                if (Timer > Interval)
                {
                    CurrentFrameX++;
                    Timer = 0;
                }
                if (CurrentFrameX == 11)
                {
                    CurrentFrameX = 0;
                    
                }
            
        }

        public void Draw(SpriteBatch spriteBatch)
        { 
            spriteBatch.Begin();
            spriteBatch.Draw(PlayerSpreadSheet, PlayerRectangle, SourceFrame, Color.White);
            spriteBatch.End();
        
        }






    }
}
