﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace One_Man_s_Land
{
    class BulletManager
    {
        public List<ICollisionable> ListBullet = new List<ICollisionable>();    //List of bullet to be taken in charge
        ContentManager content;             //content manager that will be passed to an instance of a bullet to load its texture
        CollisionManager collisionManager;          //The bullets in the list will need to be place inside de collisionManager to ckeck for collision

        public BulletManager(CollisionManager collisionManager)
        {
            this.collisionManager = collisionManager;
        }

        public void Load(ContentManager content)
        {
            this.content = content;
        }

        public void Update()
        {
            foreach (Bullet oneBullet in ListBullet.ToList())  //we get through to list to update each bullet, and remove those who's their time has expired
            {
                oneBullet.Update();

                if (oneBullet.TimeToLive < 0)
                { ListBullet.Remove(oneBullet); collisionManager.RemoveCollisionableObject(oneBullet); }  //live time has expired without touching anything

                if (oneBullet.ToRemove == true)
                { ListBullet.Remove(oneBullet); collisionManager.RemoveCollisionableObject(oneBullet); oneBullet.ToRemove = false; }   //has hit the player and must be removed
            }

        }

        //Dessin de toutes les balles se trouvant dans la liste des balles
        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Bullet oneBullet in ListBullet)
            {
                oneBullet.Draw(spriteBatch);
            }
        }

        //ShootBullet is called by an enemy - it's the only entry point and contact with the bullet system by any enemy objects
        //Ajouter une nouvelle balle de fusil à la liste des balles 
        //Cette opération ne se fait qu'une fois la gachette appuyée par l'ennemi, et non dans la méthode update
        public void ShootBullet(float playerPositionX, float playerPositionY, float enemyPositionX, float enemyPositionY, bool TriggerPulled, bool nextBullet)
        {
            if ((TriggerPulled) && (nextBullet == true))  //information coming from the enemy object
            {
                Bullet oneBullet = new Bullet();   //creates a new bullet
                oneBullet.Load(content);        //Loading bullet assets
                oneBullet.SettingUpBullet(playerPositionX, playerPositionY, enemyPositionX, enemyPositionY, true);  //preparing the bullet's trajectory with info coming from the enemy
                ListBullet.Add(oneBullet);      //adding the all set bullet to the list of bullet to be updated in the game loop

                collisionManager.AddCollisionableObject(oneBullet);     //adding the bullet to the list of collisionable objects
                
                TriggerPulled = false;  //reseting the ShootBullet method to be ready for the next bullet creation
            }

        }




    }
}
