using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace One_Man_s_Land
{
    //XNA always starts from Game1.cs
    //It is used to create the official Main and passing some core
    //objects forced to be created here by the sdk
    //e.g. Content, GraphicsDevice, GraphicsDeviceManager et SpriteBatch

    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;  //XNA Core object
        SpriteBatch spriteBatch;         //XNA Core object
        Main main = new Main();          //New practical entry point to the game
       
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        
        protected override void Initialize()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            main.Initialize(GraphicsDevice, graphics, Content);
            base.Initialize();
        }
        
        protected override void LoadContent()
        {
            main.Load(Content);
        }

        protected override void UnloadContent()
        {}
                
        protected override void Update(GameTime gameTime)
        {
            //To force exit game - need to be called from Game1.cs 
            KeyboardState ks = Keyboard.GetState();
            if ((ks.IsKeyDown(Keys.Escape)) || (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed))
                this.Exit();
            
            main.Update(gameTime);
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            main.Draw(spriteBatch);
            base.Draw(gameTime);
        }
    }
}
