﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace One_Man_s_Land
{
    public class ParticleEngine
    {


        public Vector2 EmitterLocation;
        private List<Particle> particles;
        private Texture2D texture;
        private Random random;
        public int timer;
        

        public ParticleEngine(Texture2D texture)
        {
            this.texture = texture;
            particles = new List<Particle>();
            random = new Random();
            timer = 0;
            
            
        }

        
        private Particle GenerateNewParticle(Vector2 position, Vector2 enemyPosition)
        {
            EmitterLocation = position;
            Vector2 velocity = new Vector2();

            if (enemyPosition.X > position.X)
            {
                velocity = new Vector2(
                       1f * (float)(random.NextDouble() * -5), 
                       2.5f * (float)(random.NextDouble() * 1f));
            }

            if (enemyPosition.X < position.X)
            {
                velocity = new Vector2(
                       1f * (float)(random.NextDouble() * 5),
                       2.5f * (float)(random.NextDouble() * 1f));
            }
            
            
            float angle = 270;
            float angularVelocity = 0.2f * (float)(random.NextDouble() * 2 - 1);
            Color color = new Color(
                    (float)random.NextDouble(),
                    (float)random.NextDouble(),
                    (float)random.NextDouble());
            float size = (float)random.NextDouble();
            int ttl = 5 + random.Next(40);

            return new Particle(texture, EmitterLocation, velocity, angle, angularVelocity, color, size, ttl);
        }

        
        public void Update(int total, Vector2 position, Vector2 enemyPosition)
        {
           
                if (timer > 0)
                {
                    for (int i = 0; i < total; i++)
                    {
                        particles.Add(GenerateNewParticle(position, enemyPosition));
                    }
                    timer--;
                }

                for (int particle = 0; particle < particles.Count; particle++)
                {
                    particles[particle].Update();
                    if (particles[particle].TTL <= 0)
                    {
                        particles.RemoveAt(particle);
                        particle--;
                        
                    }

                }
              
                       
        }

        public void Draw(SpriteBatch spriteBatch)
        {
             
            for (int index = 0; index < particles.Count; index++)
            {
                particles[index].Draw(spriteBatch);
            }
            
        }


    }
}
