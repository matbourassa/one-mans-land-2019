﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace One_Man_s_Land
{
    class CollisionManager
    {

        public List<ICollisionable> collisionableObjectList = new List<ICollisionable>();
        

       public void AddCollisionableObject(ICollisionable item)
        {

            collisionableObjectList.Add(item);

        }


        public void RemoveCollisionableObject(ICollisionable item)
        {
            collisionableObjectList.Remove(item);
        }

        
        public void LoopCollisionDetection()
        {

            for (int i = 0; i < collisionableObjectList.Count; i++)
            {
                for (int j = 0; j < collisionableObjectList.Count; j++)
                {
                    
                    CollisionDetection(collisionableObjectList[i], collisionableObjectList[j]);
                }
             }
            
        }


        public void CollisionDetection(ICollisionable object1, ICollisionable object2)
        {
            if (object1.GetCollisionRectangle() != object2.GetCollisionRectangle()) //pour éviter qu'un objet se collisionne avec lui même
            {
                if (object1.GetCollisionRectangle().Intersects(object2.GetCollisionRectangle()))
                {
                    object1.ReactionToCollision(object2.GetType(), object2.GetPosition(), object2.GetState());
                    object2.ReactionToCollision(object1.GetType(), object1.GetPosition(), object1.GetState());
                }
                
            }
        }

                

    }
}
