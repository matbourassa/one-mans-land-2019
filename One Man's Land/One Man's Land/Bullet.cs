﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace One_Man_s_Land
{
    class Bullet : ICollisionable
    {
        //Physical specs of the bullet
        public Vector2 BulletVelocity;
        public Vector2 BulletPosition;
        int BulletHeight;
        int BulletWidth;
        Texture2D BulletTextureLeft;
        Texture2D BulletTextureRight;

        //State specs of the bullet
        public bool BulletOut; //Est-ce que la Bullet est sortie du fusil?
        public float TimeToLive; //Le temps avant que la Bullet soit retiré de la liste sans qu'elle touche à quoi que ce soit
        public bool ToRemove;  //Triggering the remove part of the logic
        public Rectangle BulletDetectionRectangle;
        public AnimationState bulletState = new AnimationState();   //State is used to tell in which direction the bullet is flying
        Type bulletType;        //Type is used by the collision manager 
        
        public void Load(ContentManager content)
        {
            bulletType = new Type();
            bulletType = Type.Bullet;
            BulletTextureLeft = content.Load<Texture2D>("EnemyType1/BulletLeft");
            BulletTextureRight = content.Load<Texture2D>("EnemyType1/BulletRight");
            BulletOut = false;
            TimeToLive = 160;
            BulletHeight = 8;
            BulletWidth = 20;
            ToRemove = false;
        }

        public void Update()
        {
            //Rectangle used to valid collision with other game objects
            BulletDetectionRectangle = new Rectangle((int)BulletPosition.X, (int)BulletPosition.Y, BulletWidth, BulletHeight);

            if (BulletOut)
            {
                //Calculer la position de la balle en fonction de sa velocité
                BulletPosition += BulletVelocity;
                TimeToLive--;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (BulletOut)
            {
                //Drawing the right texture according to the direction of the bullet  pointing right or left
                if (bulletState == AnimationState.FlyingToTheLeft)
                    spriteBatch.Draw(BulletTextureLeft, BulletPosition, Color.White);

                if (bulletState == AnimationState.FlyingToTheRight)
                    spriteBatch.Draw(BulletTextureRight, BulletPosition, Color.White);
            }
        }

        //SettingUpBullet is called by the bulletManager once to create an instance of a bullet everytime an enemy pull the trigger
        public void SettingUpBullet(float playerPositionX, float playerPositionY, float enemyPositionX, float enemyPositionY, bool BulletOut)
        {
            //this information's coming from the bullet manager
            this.BulletOut = BulletOut;

            //Rebâtir les deux vectors de position
            Vector2 playerPosition = new Vector2(playerPositionX, playerPositionY);
            Vector2 enemyType1Position = new Vector2(enemyPositionX, enemyPositionY);

            //Vérifier la direction à laquelle la balle va partir et lui donner le bon état
            if (playerPositionX < enemyPositionX)
                bulletState = AnimationState.FlyingToTheLeft;

            if (playerPositionX > enemyPositionX)
                bulletState = AnimationState.FlyingToTheRight;

            //S'assurer que la balle debute sa course dans le fusil
            BulletPosition = enemyType1Position;

            //calculer l'angle de la balle entre l'enemie et le joueur
            Vector2 direction = new Vector2(playerPosition.X - enemyType1Position.X, playerPosition.Y - enemyType1Position.Y);
            BulletVelocity = Vector2.Normalize(direction);

            //Donner la vitesse voulue à la balle
            BulletVelocity = BulletVelocity * 11;
        }

        //Les 4 ICollisionable methodes
        public Vector2 GetPosition()
        {
            return BulletPosition;
        }

        public Rectangle GetCollisionRectangle()
        {
            return BulletDetectionRectangle;
        }


        public new Type GetType()
        {
            return bulletType;
        }

        public AnimationState GetState()
        {
            return bulletState;
        }

        public void ReactionToCollision(Type type, Vector2 Position, AnimationState playerState)
        {
            if (type == Type.Player)   //if the bullet hit the player, this bullet is taken out of the game - the player will manage his own reaction the this collision
            {
                BulletOut = false;
                ToRemove = true;
            }
        }
    }
}
