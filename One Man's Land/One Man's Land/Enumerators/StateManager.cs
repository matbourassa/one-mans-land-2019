﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace One_Man_s_Land
{

    //Etat de l<animation du heros ou d<un ennemi
    public enum AnimationState
    {
        isStandingRight,
        isStandingLeft,
        isWalkingRight,
        isWalkingLeft,
        isRunningRight,
        isRunningLeft,
        isCrouchingRight,
        isCrouchingLeft,
        isUnCrouchingRight,
        isUnCrouchingLeft,
        isLowPunchRight,
        isLowPunchLeft,
        isHighPunchRight,
        isHighPunchLeft,
        hasStartShootingRight,
        isDoneShootingRight,
        hasStartShootingLeft,
        isDoneShootingLeft,
        isPatrolling,
        isPositioning,
        isReadyToShoot,
        isChasing,
        isReloading,
        FlyingToTheLeft,
        FlyingToTheRight,

    };

    //Etat du saut du heros et d<un ennemi
    public enum JumpState
    {
        noJump,
        isJumpingRight,
        isJumpingLeft,
        isLandingRight,
        isLandingLeft,

    };

    //Etat physique du heros ou d<un ennemi
    public enum EnergyState
    {
        isAlive,
        isDead,
        isDyingRight,
        isDyingLeft,
        hasBeenHitRight,
        hasBeenHitLeft,
        isBleeding,

    };

    //Est-ce que la balle de fusil entre en contact avec le Shield
    public enum CollisionShieldState
    {
        hasBeenHit,
        hasNotBeenHit,
        isReactingToHitRight,
        isReactingToHitLeft,
    };

    //Gestion de l>amimation du bouclier
    public enum AnimationShieldState
    {
        ShieldOn,
        ShieldOff,
        ShieldIn,
        ShieldOut,
    };

    

}
