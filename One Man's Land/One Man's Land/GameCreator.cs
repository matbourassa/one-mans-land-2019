﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace One_Man_s_Land
{
    class GameCreator    //Creates the game once
    {
        GraphicsDevice graphicsDevice;      //Needed core XNA object pass by the constructor
        GraphicsDeviceManager graphics;     //Needed core XNA object pass by the constructor

        public LevelTest levelTest1;        //Creation of the different levels - the game state will manage which one loop in the game loop
        public LevelTest levelTest2;
        
        public GameCreator(GraphicsDeviceManager graphics, GraphicsDevice graphicsDevice)
        {
            this.graphicsDevice = graphicsDevice;
            this.graphics = graphics;
        }

        public void Initialize(ContentManager content)
        {
            //Full screen + resolution (only one supported so far)
            graphics.PreferredBackBufferWidth = (1920);
            graphics.PreferredBackBufferHeight = (1080);
            graphics.IsFullScreen = true;
            graphics.ApplyChanges();

            GameState.Initialize();   //Initializing the starting state of the game
            
            SoundFx.Load(content);  //will eventualy be transfert inside the Load method of each level to avoid loading every sound effects here all at once

            //TO DO//
            //every levels are created and loaded here at launch for now - with the growing of the game, the loading part will probably be managed differently
            //to control the initial app loading time

            levelTest1 = new LevelTest();
            levelTest1.Initialize(graphicsDevice);  
            levelTest1.Load(content);

            levelTest2 = new LevelTest();
            levelTest2.Initialize(graphicsDevice); 
            levelTest2.Load(content);
        }
    }
}
