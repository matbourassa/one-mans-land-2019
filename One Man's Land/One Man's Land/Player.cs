﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;

namespace One_Man_s_Land
{
    class Player : ICollisionable
    {


        //Needed add-on of the hero
        public Shield shield;
        public ParticleEngine bloodParticles;
        Texture2D blood;

        //Helping class
        AnimationHelper player1AnimationHelper;
        AnimationHelper player2AnimationHelper;
        AnimationHelper player3AnimationHelper;
        AnimationHelper player4AnimationHelper;
        CollisionManager currentCollisionManager;
        CollisionGridLevelTest collisionGridLevelTest;
        GamePadState oldPadState;
        bool ControllerActive;

        //Physical specs of the hero
        Texture2D PlayerSpreadSheet1;
        Texture2D PlayerSpreadSheet2;
        public Vector2 PlayerPosition;
        Vector2 OldPlayerPosition;
        public Vector2 PlayerVelocity;
        Rectangle PlayerRectangle;
        public BoundingSphere positionSphere;
        const int PlayerSmallWidth = 192;
        const int PlayerBigWidth = 256;
        const int PlayerHeight = 256;
        float JumpSpeed;
        float LandingTimer;

        //State specs of the hero
        JumpState jumpState;
        AnimationState animationState;
        public EnergyState playerEnergyState;
        CollisionShieldState collisionShieldState;
        Type playerType;
        string IsLookingToThe;
        int DrawTurn = 1;
        bool hasjump;
        bool isCrouch;
        bool hashit;

        public Player(CollisionManager currentCollisionManager)
        {
            this.currentCollisionManager = currentCollisionManager;
        }

        public void Initialize()
        {
            //Add-on class
            shield = new Shield();

            //Helping class
            player1AnimationHelper = new AnimationHelper(PlayerSmallWidth, PlayerHeight);
            player2AnimationHelper = new AnimationHelper(PlayerBigWidth, PlayerHeight);
            player3AnimationHelper = new AnimationHelper(PlayerSmallWidth, PlayerHeight);
            player4AnimationHelper = new AnimationHelper(PlayerBigWidth, PlayerHeight);
            collisionGridLevelTest = new CollisionGridLevelTest();
            ControllerActive = true;

            //Physical specs class
            PlayerPosition = new Vector2(350, 920);
            PlayerVelocity = new Vector2(0, 0);
            IsLookingToThe = "Right";
            LandingTimer = 0;
            JumpSpeed = -30;

            //State specs class
            jumpState = new JumpState();
            animationState = new AnimationState();
            playerEnergyState = new EnergyState();
            collisionShieldState = new CollisionShieldState();
            collisionShieldState = CollisionShieldState.hasNotBeenHit;
            playerType = new Type();
            playerType = Type.Player;
            jumpState = JumpState.noJump;
            hasjump = false;
            isCrouch = false;
            hashit = false;
        }

        public void Load(ContentManager content)
        {
            //Add-on class
            blood = content.Load<Texture2D>("Player/Blood");
            bloodParticles = new ParticleEngine(blood);
            shield.Load(content);

            //Physical specs class
            PlayerSpreadSheet1 = content.Load<Texture2D>("Player/Player1SpreadSheet");
            PlayerSpreadSheet2 = content.Load<Texture2D>("Player/Player2SpreadSheet");
            player1AnimationHelper.Load(PlayerSpreadSheet1);
            player2AnimationHelper.Load(PlayerSpreadSheet2);
            player3AnimationHelper.Load(PlayerSpreadSheet1);
            player4AnimationHelper.Load(PlayerSpreadSheet2);
        }

        public void Update(GameTime gameTime, Texture2D FocalTexture)
        {
            //Helping class
            shield.Update(gameTime, PlayerPosition, IsLookingToThe);
            ShieldStateReaction(gameTime);
            BloodReaction();

            //Physical specs class
            PlayerPositionCorrection();
            GravityPull();
            LoadSegment(FocalTexture); //doit rester ici pour le moment -  a deplacer plus tard - detection avec le plancher - to be taken out
            //FloorCollisionDetection(FocalTexture); //en train de remplacerle loadsegment

            //State specs class
            StateModification();
            AnimationStateReaction(gameTime);
            JumpStateReaction(gameTime);
            EnergyStateReaction(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            bloodParticles.Draw(spriteBatch);

            if (DrawTurn == 1)
                player1AnimationHelper.Draw(spriteBatch, PlayerPosition);
            if (DrawTurn == 2)
                player2AnimationHelper.Draw(spriteBatch, PlayerPosition);
            if (DrawTurn == 3)
                player3AnimationHelper.Draw(spriteBatch, PlayerPosition);
            if (DrawTurn == 4)
                player4AnimationHelper.Draw(spriteBatch, PlayerPosition);

            shield.DrawHit(spriteBatch);
        }

        //Calculate the new position of the player accordind to its velocity + redrawing of the collision rectangle using this position
        void PlayerPositionCorrection()
        {
            OldPlayerPosition = PlayerPosition;
            PlayerPosition += PlayerVelocity;
            PlayerVelocity.X = 0;
            PlayerRectangle = new Rectangle((int)PlayerPosition.X + 84, (int)PlayerPosition.Y + 24, 24, 148);  //Rectangle de 24 de large par 148 de haut
            positionSphere = new BoundingSphere(new Vector3(PlayerPosition.X + (PlayerSmallWidth / 2), PlayerPosition.Y + (PlayerHeight / 2), 0), (PlayerHeight / 2));

        }

        //Pull the player the following gravity rules
        void GravityPull()
        {
            if (PlayerVelocity.Y < 10)
                PlayerVelocity.Y += 10;
        }

        //Updating the creation and position of blood particles if triggered 
        void BloodReaction()
        {
            foreach (var item in currentCollisionManager.collisionableObjectList)
            {
                if (item.GetType() == Type.EnemyType1) //Need to know the enemy position to spread to blood in the right direction
                {
                    if (IsLookingToThe == "Right")
                    {
                        bloodParticles.Update(100, new Vector2(PlayerPosition.X + 100, PlayerPosition.Y + 90), item.GetPosition());
                    }
                    if (IsLookingToThe == "Left")
                    {
                        bloodParticles.Update(100, new Vector2(PlayerPosition.X + 100, PlayerPosition.Y + 90), item.GetPosition());
                    }
                }
            }

        }


        private void FloorCollisionDetection(Texture2D LevelTexture)
        {
            //Détection de collision 
            collisionGridLevelTest.VerifyHorizontalCollision(PlayerPosition, LevelTexture, PlayerRectangle, PlayerVelocity.X);

            if (hasjump == true)
            {
                if (IsLookingToThe == "Right")
                    jumpState = JumpState.isLandingRight;
                if (IsLookingToThe == "Left")
                    jumpState = JumpState.isLandingLeft; //Reste ce bout la a extraire de cette methode...
            }

            if (hasjump == false)
            {
                JumpSpeed = -30; //Reset de la vitesse en prevision du prochain saut
            }
            //Ajustement de la vélocité, et de la position verticale et horizontale suite à la détection de collision 
            PlayerPosition.Y = collisionGridLevelTest.AdjustHorizontalePosition();
            PlayerPosition.X = collisionGridLevelTest.AdjustVerticalePosition();
            PlayerVelocity.X = collisionGridLevelTest.AdjustVerticaleVelocity();

            

        }

        //Coordinate to constitute the floor level
        void LoadSegment(Texture2D FocalTexture)
        {
            var CoordonneList = new List<Tuple<Vector2, Vector2>>
            {
                new Tuple<Vector2, Vector2>(new Vector2(0, 1200), new Vector2(200, 1175)),
                new Tuple<Vector2, Vector2>(new Vector2(200, 1175), new Vector2(600, 1150)),
                new Tuple<Vector2, Vector2>(new Vector2(600, 1150), new Vector2(1500, 1000)),
                new Tuple<Vector2, Vector2>(new Vector2(1500, 1000), new Vector2(2000, 910)),

                new Tuple<Vector2, Vector2>(new Vector2(1900, 1120), new Vector2(2020, 1140)),
                new Tuple<Vector2, Vector2>(new Vector2(2020, 1140), new Vector2(2900, 1110)),
                new Tuple<Vector2, Vector2>(new Vector2(2900, 1110), new Vector2(3400, 1140)),
                new Tuple<Vector2, Vector2>(new Vector2(3400, 1140), new Vector2(3790, 1170)),
                new Tuple<Vector2, Vector2>(new Vector2(3790, 1170), new Vector2(4000, 1170)),
            };

            Rectangle DecorRecangle1 = new Rectangle(1760, 990, 200, 300);

            foreach (Tuple<Vector2, Vector2> Coordonne in CoordonneList)
            {
                Vector2 Start;
                Vector2 End;
                Start = Coordonne.Item1;
                End = Coordonne.Item2;

                LineCollision(Start, End, DecorRecangle1, FocalTexture);
            }
        }

        //second part of the floor detection system
        void LineCollision(Vector2 StartSeg, Vector2 EndSeg, Rectangle Decor1, Texture2D FocalTexture)
        {
            if ((PlayerPosition.X + 100 > StartSeg.X) && ((PlayerPosition.X + 60) < EndSeg.X))
            {
                //Y = mx + c

                float m = (EndSeg.Y - StartSeg.Y) / (EndSeg.X - StartSeg.X); // Pente
                float c = StartSeg.Y - (m * StartSeg.X); // Y Intercept

                //Résolution de l'équation
                if (((PlayerPosition.Y + 256) > (m * PlayerPosition.X + c)) && ((PlayerPosition.Y + 256) < (m * PlayerPosition.X + c) + 48)) //Une mince bande sous le rail dans laquelle la collision se résoud.  Comme ça, on peut passer sous une ligne.
                {
                    PlayerPosition.Y = ((m * PlayerPosition.X + c) - 240);

                    if (hasjump == true)
                    {
                        if (IsLookingToThe == "Right")
                            jumpState = JumpState.isLandingRight;
                        if (IsLookingToThe == "Left")
                            jumpState = JumpState.isLandingLeft; //Reste ce bout la a extraire de cette methode...
                    }

                    if (hasjump == false)
                    {
                        JumpSpeed = -30; //Reset de la vitesse en prevision du prochain saut
                    }
                }
            }

            if (PlayerRectangle.Intersects(Decor1))
            {
                PlayerPosition.X = OldPlayerPosition.X;//1.1f;
                
            }

            if (PlayerPosition.X < 0) PlayerPosition.X = 0;
            if (PlayerPosition.X > FocalTexture.Width - (PlayerRectangle.Width * 4)) PlayerPosition.X = FocalTexture.Width - (PlayerRectangle.Width * 4);
        }

        //Get the modification of the state of the player, wether from the controller or from the environment
        void StateModification()
        {
            ControllerModification();
            EnvironmentModification();
        }

        void ControllerModification()
        {
            GamePadState gamePadState = GamePad.GetState(PlayerIndex.One);  //movement of the hero
            shield.ShieldController(gamePadState, oldPadState);             //particular control for the shield power

            if (ControllerActive)
            {
                //Ramener l<etat du perso au neutre si le joystick n<est pas touche
                if ((animationState == AnimationState.isWalkingRight) && (gamePadState.ThumbSticks.Left.X == 0))
                { animationState = AnimationState.isStandingRight; hashit = false; }

                if ((animationState == AnimationState.isWalkingLeft) && (gamePadState.ThumbSticks.Left.X == 0))
                { animationState = AnimationState.isStandingLeft; hashit = false; }

                if ((animationState == AnimationState.isRunningRight) && (gamePadState.ThumbSticks.Left.X == 0))
                { animationState = AnimationState.isStandingRight; hashit = false; }

                if ((animationState == AnimationState.isRunningLeft) && (gamePadState.ThumbSticks.Left.X == 0))
                { animationState = AnimationState.isStandingLeft; hashit = false; }

                //S assurer que l animation CROUCH se reset
                if ((gamePadState.ThumbSticks.Left.Y >= -0.5) && (oldPadState.ThumbSticks.Left.Y < -0.2))
                {
                    player1AnimationHelper.SetCurrentFrame(0); player3AnimationHelper.SetCurrentFrame(0);
                }

                //Marcher vers la droite
                if ((gamePadState.ThumbSticks.Left.X > 0) && (gamePadState.ThumbSticks.Left.X <= 0.9))
                {
                    if (!isCrouch)
                    {
                        animationState = AnimationState.isWalkingRight;
                    }
                }

                //Marcher vers la gauche
                if ((gamePadState.ThumbSticks.Left.X < 0) && (gamePadState.ThumbSticks.Left.X >= -0.9))
                {
                    if (!isCrouch)
                    {
                        animationState = AnimationState.isWalkingLeft;
                    }
                }

                //Courrir vers la droite
                if ((gamePadState.ThumbSticks.Left.X > 0.9))
                {
                    if (!isCrouch)
                    {
                        animationState = AnimationState.isRunningRight;
                    }
                }

                //Courrir vers la gauche
                if ((gamePadState.ThumbSticks.Left.X < -0.9))
                {
                    if (!isCrouch)
                    {
                        animationState = AnimationState.isRunningLeft;
                    }
                }

                //Se relever
                if ((gamePadState.ThumbSticks.Left.Y > -0.5) && (oldPadState.ThumbSticks.Left.Y < -0.5))
                {
                    if (IsLookingToThe == "Right")
                        animationState = AnimationState.isUnCrouchingRight;

                    if (IsLookingToThe == "Left")
                        animationState = AnimationState.isUnCrouchingLeft;
                }

                //Se pencher
                if ((gamePadState.ThumbSticks.Left.Y < -0.5))
                {
                    if (IsLookingToThe == "Right")
                    {
                        animationState = AnimationState.isCrouchingRight;
                        isCrouch = true;
                    }

                    if (IsLookingToThe == "Left")
                    {
                        animationState = AnimationState.isCrouchingLeft;
                        isCrouch = true;
                    }
                }

                //HitHighPunch
                if (!hashit)
                {
                    if ((gamePadState.Buttons.X == ButtonState.Pressed) && (oldPadState.Buttons.X == ButtonState.Released))
                    {
                        if (!isCrouch)
                        {
                            if (IsLookingToThe == "Right")
                            {
                                animationState = AnimationState.isHighPunchRight;
                            }

                            if (IsLookingToThe == "Left")
                            {
                                animationState = AnimationState.isHighPunchLeft;
                            }
                        }
                    }
                }

                //HitLowPunch
                if (!hashit)
                {
                    if ((gamePadState.Buttons.Y == ButtonState.Pressed) && (oldPadState.Buttons.Y == ButtonState.Released))
                    {
                        if (!isCrouch)
                        {
                            if (IsLookingToThe == "Right")
                            {
                                animationState = AnimationState.isLowPunchRight;
                            }

                            if (IsLookingToThe == "Left")
                            {
                                animationState = AnimationState.isLowPunchLeft;
                            }
                        }
                    }
                }

                //Sauter
                if (!hasjump)
                {
                    if ((gamePadState.Buttons.A == ButtonState.Pressed) && (oldPadState.Buttons.A == ButtonState.Released))
                    {
                        player3AnimationHelper.SetCurrentFrame(0);
                        //collisionGridLevelTest.SetOnTheFloor(false); //Le player lache le sol - c.est la classe CollisionGridLevelTest qui la remettra a true
                        if (IsLookingToThe == "Right")
                        { jumpState = JumpState.isJumpingRight; player1AnimationHelper.SetCurrentFrame(0); }
                        if (IsLookingToThe == "Left")
                        { jumpState = JumpState.isJumpingLeft; player1AnimationHelper.SetCurrentFrame(0); }
                    }
                }

                //Conserver l etat du gamepad pour le loop suivante
                oldPadState = gamePadState;
            }
        }

        //Nothing for now - to be populated
        void EnvironmentModification()
        { }

        //Once the player state has been modified by the StateModification() method, the reaction is taken in charge here
        void AnimationStateReaction(GameTime gameTime)
        {
            switch (animationState)
            {
                case AnimationState.isStandingRight:
                    IsStandingRight(gameTime);
                    break;

                case AnimationState.isStandingLeft:
                    IsStandingLeft(gameTime);
                    break;

                case AnimationState.isWalkingRight:
                    IsWalkingRight(gameTime);
                    break;

                case AnimationState.isWalkingLeft:
                    IsWalkingLeft(gameTime);
                    break;

                case AnimationState.isRunningRight:
                    IsRunningRight(gameTime);
                    break;

                case AnimationState.isRunningLeft:
                    IsRunningLeft(gameTime);
                    break;

                case AnimationState.isCrouchingRight:
                    IsCrouchingRight(gameTime);
                    break;

                case AnimationState.isCrouchingLeft:
                    IsCrouchingLeft(gameTime);
                    break;

                case AnimationState.isUnCrouchingRight:
                    IsUnCrouchingRight(gameTime);
                    break;

                case AnimationState.isUnCrouchingLeft:
                    IsUnCrouchingLeft(gameTime);
                    break;

                case AnimationState.isHighPunchRight:
                    IsHighPunchRight(gameTime);
                    break;

                case AnimationState.isHighPunchLeft:
                    IsHighPunchLeft(gameTime);
                    break;

                case AnimationState.isLowPunchRight:
                    IsLowPunchRight(gameTime);
                    break;

                case AnimationState.isLowPunchLeft:
                    IsLowPunchLeft(gameTime);
                    break;
            }
        }

        //Again, the reaction is taken in charge here, but for the jump state particulary (since the jump and movement states can sometime overlap
        void JumpStateReaction(GameTime gameTime)
        {
            switch (jumpState)
            {
                case JumpState.isJumpingRight:
                    IsJumpingRight(gameTime);
                    break;

                case JumpState.isJumpingLeft:
                    IsJumpingLeft(gameTime);
                    break;

                case JumpState.isLandingRight:
                    IsLandingRight(gameTime);
                    break;

                case JumpState.isLandingLeft:
                    IsLandingLeft(gameTime);
                    break;

                case JumpState.noJump:
                    NoJump(gameTime);
                    break;
            }
        }

        //Reaction to the different energy states of the player
        void EnergyStateReaction(GameTime gameTime)
        {
            switch (playerEnergyState)
            {
                case EnergyState.isAlive:
                    IsAlive(gameTime);
                    break;

                case EnergyState.isDead:
                    IsDead(gameTime);
                    break;

                case EnergyState.isDyingRight:
                    IsDyingRight();
                    break;

                case EnergyState.isDyingLeft:
                    IsDyingLeft();
                    break;

                case EnergyState.hasBeenHitRight:
                    HasBeenHitRight();
                    break;

                case EnergyState.hasBeenHitLeft:
                    HasBeenHitLeft();
                    break;

                case EnergyState.isBleeding:
                    IsBleeding();
                    break;
            }
        }

        //Reaction of the shield
        void ShieldStateReaction(GameTime gameTime)
        {
            switch (collisionShieldState)
            {
                case CollisionShieldState.hasBeenHit:
                    ShieldHasBeenHit();
                    break;

                case CollisionShieldState.isReactingToHitRight:
                    ShieldIsReactingToHitRight(gameTime);
                    break;

                case CollisionShieldState.isReactingToHitLeft:
                    ShieldIsReactingToHitLeft(gameTime);
                    break;

                case CollisionShieldState.hasNotBeenHit:
                    ShieldHasNotBeenHit(gameTime);
                    break;
            }
        }

        //In the next methods stand the logic for the different reactions of the hero
        void IsStandingRight(GameTime gameTime)
        {
            DrawTurn = 1;
            IsLookingToThe = "Right";
            PlayerVelocity.X = 0;
            player1AnimationHelper.Update(gameTime, 60, 0, 20, false);
        }

        void IsStandingLeft(GameTime gameTime)
        {
            DrawTurn = 1;
            IsLookingToThe = "Left";
            PlayerVelocity.X = 0;
            player1AnimationHelper.Update(gameTime, 60, 1, 20, false);
        }

        void IsWalkingRight(GameTime gameTime)
        {
            DrawTurn = 1;
            IsLookingToThe = "Right";
            PlayerVelocity.X = 3.2f;
            player1AnimationHelper.Update(gameTime, 37, 2, 20, false);
        }

        void IsWalkingLeft(GameTime gameTime)
        {
            DrawTurn = 1;
            IsLookingToThe = "Left";
            PlayerVelocity.X = -3.2f;
            player1AnimationHelper.Update(gameTime, 37, 3, 20, false);
        }

        void IsRunningRight(GameTime gameTime)
        {
            DrawTurn = 2;
            IsLookingToThe = "Right";
            PlayerVelocity.X = 12;
            player2AnimationHelper.Update(gameTime, 65, 0, 12, false);
        }

        void IsRunningLeft(GameTime gameTime)
        {
            DrawTurn = 2;
            IsLookingToThe = "Left";
            PlayerVelocity.X = -12;
            player2AnimationHelper.Update(gameTime, 65, 1, 12, false);
        }

        void IsCrouchingRight(GameTime gameTime)
        {
            DrawTurn = 1;
            player1AnimationHelper.Update(gameTime, 10, 4, 20, true);
        }

        void IsCrouchingLeft(GameTime gameTime)
        {
            DrawTurn = 1;
            player1AnimationHelper.Update(gameTime, 10, 5, 20, true);
        }

        void IsUnCrouchingRight(GameTime gameTime)
        {
            DrawTurn = 3;
            player3AnimationHelper.Update(gameTime, 10, 6, 20, false);
            if (player3AnimationHelper.GetAnimationIsDone() == true)
            { isCrouch = false; animationState = AnimationState.isStandingRight; player3AnimationHelper.SetCurrentFrame(19); player3AnimationHelper.SetAnimationIsdone(false); }
        }

        void IsUnCrouchingLeft(GameTime gameTime)
        {
            DrawTurn = 3;
            player3AnimationHelper.Update(gameTime, 10, 7, 20, false);
            if (player3AnimationHelper.GetAnimationIsDone() == true)
            { isCrouch = false; animationState = AnimationState.isStandingLeft; player3AnimationHelper.SetCurrentFrame(19); player3AnimationHelper.SetAnimationIsdone(false); }
        }

        void IsLowPunchRight(GameTime gameTime)
        {
            DrawTurn = 4;
            hashit = true;
            ControllerActive = false;
            IsLookingToThe = "Right";
            PlayerVelocity.X = 0f;
            player4AnimationHelper.Update(gameTime, 30, 4, 15, false);
            if (player4AnimationHelper.GetAnimationIsDone() == true)
            {
                hashit = false; player4AnimationHelper.SetAnimationIsdone(false); player4AnimationHelper.SetCurrentFrame(0); animationState = AnimationState.isStandingRight;
                ControllerActive = true;
            }
        }

        void IsLowPunchLeft(GameTime gameTime)
        {
            DrawTurn = 4;
            hashit = true;
            ControllerActive = false;
            IsLookingToThe = "Left";
            PlayerVelocity.X = 0f;
            player4AnimationHelper.Update(gameTime, 30, 5, 15, false);
            if (player4AnimationHelper.GetAnimationIsDone() == true)
            {
                hashit = false; player4AnimationHelper.SetAnimationIsdone(false); player4AnimationHelper.SetCurrentFrame(0); animationState = AnimationState.isStandingLeft;
                ControllerActive = true;
            }
        }

        void IsHighPunchRight(GameTime gameTime)
        {
            DrawTurn = 4;
            hashit = true;
            ControllerActive = false;
            IsLookingToThe = "Right";
            PlayerVelocity.X = 0f;
            player4AnimationHelper.Update(gameTime, 45, 2, 15, false);
            if (player4AnimationHelper.GetAnimationIsDone() == true)
            {
                hashit = false; player4AnimationHelper.SetAnimationIsdone(false); player4AnimationHelper.SetCurrentFrame(0); animationState = AnimationState.isStandingRight;
                ControllerActive = true;
            }
        }

        void IsHighPunchLeft(GameTime gameTime)
        {
            DrawTurn = 4;
            hashit = true;
            ControllerActive = false;
            IsLookingToThe = "Left";
            PlayerVelocity.X = 0f;
            player4AnimationHelper.Update(gameTime, 45, 3, 15, false);
            if (player4AnimationHelper.GetAnimationIsDone() == true)
            {
                hashit = false; player4AnimationHelper.SetAnimationIsdone(false); player4AnimationHelper.SetCurrentFrame(0); animationState = AnimationState.isStandingLeft;
                ControllerActive = true;
            }
        }

        void IsJumpingRight(GameTime gameTime)
        {
            DrawTurn = 3;
            hasjump = true;
            IsLookingToThe = "Right";
            PlayerPosition.Y += JumpSpeed;
            JumpSpeed += 1;
            player3AnimationHelper.Update(gameTime, 30, 8, 15, true);
        }

        void IsJumpingLeft(GameTime gameTime)
        {
            DrawTurn = 3;
            hasjump = true;
            IsLookingToThe = "Left";
            PlayerPosition.Y += JumpSpeed;
            JumpSpeed += 1;
            player3AnimationHelper.Update(gameTime, 30, 9, 15, true);
        }

        void IsLandingRight(GameTime gameTime)
        {
            DrawTurn = 1;
            ControllerActive = false;
            PlayerVelocity.X = 0f;
            IsLookingToThe = "Right";
            LandingTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            jumpState = JumpState.noJump;
            player1AnimationHelper.Update(gameTime, 2, 10, 20, false);

            if (LandingTimer > 100)
            {
                player1AnimationHelper.SetAnimationIsdone(false);
                LandingTimer = 0;
                hasjump = false;
                player3AnimationHelper.SetCurrentFrame(0);
                player1AnimationHelper.SetCurrentFrame(19);
                ControllerActive = true;
            }
        }

        void IsLandingLeft(GameTime gameTime)
        {
            DrawTurn = 1;
            ControllerActive = false;
            PlayerVelocity.X = 0f;
            IsLookingToThe = "Left";
            LandingTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            jumpState = JumpState.noJump;
            player1AnimationHelper.Update(gameTime, 2, 11, 20, false);

            if (LandingTimer > 100)
            {
                player1AnimationHelper.SetAnimationIsdone(false);
                LandingTimer = 0;
                hasjump = false;
                player3AnimationHelper.SetCurrentFrame(0);
                player1AnimationHelper.SetCurrentFrame(19);
                ControllerActive = true;
            }
        }

        void NoJump(GameTime gameTime)
        { }

        void IsAlive(GameTime gameTime)
        { }

        void IsDead(GameTime gameTime)
        { }

        void HasBeenHitRight()
        {
            bloodParticles.timer = 5;
            playerEnergyState = EnergyState.isBleeding;
        }

        void HasBeenHitLeft()
        {
            bloodParticles.timer = 5;
            playerEnergyState = EnergyState.isBleeding;
        }

        void IsBleeding()
        {
            if (bloodParticles.timer == 0)
            { playerEnergyState = EnergyState.isAlive; }
        }

        void IsDyingRight()
        { }

        void IsDyingLeft()
        { }

        void ShieldHasBeenHit()
        {
            shield.HitTimer = 35; shield.Sound = true;
            if (IsLookingToThe == "Right") collisionShieldState = CollisionShieldState.isReactingToHitRight;
            if (IsLookingToThe == "Left") collisionShieldState = CollisionShieldState.isReactingToHitLeft;
        }

        void ShieldIsReactingToHitRight(GameTime gameTime)
        {
            shield.UpdateHit(gameTime, PlayerPosition.X + 158, PlayerPosition.Y + 48);
            if (!shield.UpdateHit(gameTime, PlayerPosition.X + 158, PlayerPosition.Y + 48))
            {
                collisionShieldState = CollisionShieldState.hasNotBeenHit;
            }
        }

        void ShieldIsReactingToHitLeft(GameTime gameTime)
        {
            shield.UpdateHit(gameTime, PlayerPosition.X + 18, PlayerPosition.Y + 68);
            if (!shield.UpdateHit(gameTime, PlayerPosition.X + 18, PlayerPosition.Y + 68))
            {
                collisionShieldState = CollisionShieldState.hasNotBeenHit;
            }
        }

        void ShieldHasNotBeenHit(GameTime gameTime)
        { }

        //The 5 ICollisionable methods used to managed collisions
        public Vector2 GetPosition()
        {
            return PlayerPosition;
        }

        public Rectangle GetCollisionRectangle()
        {
            return PlayerRectangle;
        }

        public new Type GetType()
        {
            return playerType;
        }

        public AnimationState GetState()
        {
            return animationState;
        }

        public void ReactionToCollision(Type type, Vector2 Position, AnimationState state)
        {
            if (type == Type.Bullet)
            {
                if (!shield.ShieldBool)
                {
                    playerEnergyState = EnergyState.hasBeenHitRight;
                }

                if (shield.ShieldBool)
                {
                    if (collisionShieldState == CollisionShieldState.hasNotBeenHit)
                    {
                        collisionShieldState = CollisionShieldState.hasBeenHit;
                    }
                }
            }
            if (type == Type.EnemyType1)
            {
               // PlayerPosition.X = OldPlayerPosition.X;  //Collision test with th enemy
            }
        }
    }
}
