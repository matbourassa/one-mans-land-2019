﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;

namespace One_Man_s_Land
{
    class CameraDynamic
    {
        Matrix transform;
        public Matrix Transform
        {
            get { return transform; }
        }

        //Cam
        public Vector2 center;
        private Viewport viewport;
        public Player player;
        

        //Position de la target
        Vector2 origin;
        Vector2 targetPosition;

        
        Vector2 targetVelocity;
        float maxTargetVelocity;
        float minTargetVelocity;
        float maxTargetPosition;
        float minTargetPosition;
        float timerLeft;
        float timerRight;

        bool newlyRight = true;
        bool newlyLeft = true;

        GamePadState oldPadState = new GamePadState();

        enum CameraState { isCentering, isCentered, isCompletlyRight, isCompletlyLeft, isGoingRight, isGoingLeft };
        CameraState cameraState = new CameraState { };



        public CameraDynamic(Viewport newViewport, Player newPlayer)
        {
            viewport = newViewport;
            player = newPlayer;
            

        } 

        public void Load()
        {
            origin.Y = player.PlayerPosition.Y;
            origin.X = player.PlayerPosition.X + 64;
            cameraState = CameraState.isCentered;
            targetVelocity = new Vector2(0);
        }


        public void Update(Vector2 playerposition, int xOffset, int yOffset, float parallaxeFactor, GameTime gameTime)
        {
            timerLeft += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            timerRight += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            
            targetPosition.Y = player.PlayerPosition.Y;

            targetVelocity = (targetVelocity + player.PlayerVelocity);
            
            maxTargetVelocity = (6);
            minTargetVelocity = (-6);
            maxTargetPosition = (400);
            minTargetPosition = (-400);

            origin.X = player.PlayerPosition.X;
            targetPosition.X += targetVelocity.X;

            
            CameraStateModification();
                        

            //Matrix transformation
            if (targetPosition.X < viewport.Width / 2)
                center.X = viewport.Width / 2;
            else if (targetPosition.X > xOffset - (viewport.Width / 2))
                center.X = xOffset - (viewport.Width / 2);
            else center.X = targetPosition.X;


            if (targetPosition.Y < viewport.Height / 2)
                center.Y = viewport.Height / 2;
            else if (targetPosition.Y > yOffset - (viewport.Height / 2))
                center.Y = yOffset - (viewport.Height / 2);
            else center.Y = targetPosition.Y;


            transform = Matrix.CreateTranslation(new Vector3(-center.X + (viewport.Width / 2), -center.Y + (viewport.Height / 2), 0) * parallaxeFactor);
                       


        }

       

        public void CameraStateModification()
        {
           
            GamePadState gamePad = GamePad.GetState(PlayerIndex.One);
                        

            if ((gamePad.DPad.Right == ButtonState.Pressed) && (oldPadState.DPad.Right == ButtonState.Pressed)) { timerRight = 0; }
            if ((gamePad.DPad.Left == ButtonState.Pressed) && (oldPadState.DPad.Left == ButtonState.Pressed)) { timerLeft = 0; }

            if ((gamePad.ThumbSticks.Left.X > 0) && (oldPadState.ThumbSticks.Left.X > 0)) { timerRight = 0; }
            if ((gamePad.ThumbSticks.Left.X < 0) && (oldPadState.ThumbSticks.Left.X < 0)) { timerRight = 0; }


            if (targetPosition.X <= origin.X + minTargetPosition)
            {
                cameraState = CameraState.isCompletlyLeft;

            }
            if (targetPosition.X >= origin.X + maxTargetPosition)
            {

                cameraState = CameraState.isCompletlyRight;
            }

            
            if ((gamePad.DPad.Right == ButtonState.Pressed) && (oldPadState.DPad.Right == ButtonState.Released))
            {

                if (cameraState == CameraState.isCompletlyRight)
                    cameraState = CameraState.isCompletlyRight;
                else
                { cameraState = CameraState.isGoingRight; timerLeft = 0; timerRight = 0; }

            }
            if ((gamePad.ThumbSticks.Left.X > 0) && (oldPadState.ThumbSticks.Left.X == 0))
            {

                if (cameraState == CameraState.isCompletlyRight)
                    cameraState = CameraState.isCompletlyRight;
                else
                { cameraState = CameraState.isGoingRight; timerLeft = 0; timerRight = 0; }

            }

           
            if ((gamePad.DPad.Left == ButtonState.Pressed) && (oldPadState.DPad.Left == ButtonState.Released))
            {

                if (cameraState == CameraState.isCompletlyLeft)
                    cameraState = CameraState.isCompletlyLeft;
                else
                { cameraState = CameraState.isGoingLeft; timerLeft = 0; timerRight = 0; }
            }
            if ((gamePad.ThumbSticks.Left.X < 0) && (oldPadState.ThumbSticks.Left.X == 0))
            {

                if (cameraState == CameraState.isCompletlyLeft)
                    cameraState = CameraState.isCompletlyLeft;
                else
                { cameraState = CameraState.isGoingLeft; timerLeft = 0; timerRight = 0; }

            }

            if (timerLeft > 2000)
            {
                if ((cameraState == CameraState.isCompletlyLeft))
                {
                    cameraState = CameraState.isCentering;
                    timerLeft = 0;

                }

            }

            if (timerRight > 2000)
            {
                if ((cameraState == CameraState.isCompletlyRight))
                {
                    cameraState = CameraState.isCentering;
                    timerRight = 0;

                }
            }
            
            oldPadState = gamePad;
            CameraStateReaction();

        }

        
        public void CameraStateReaction()
        {


            if (cameraState == CameraState.isGoingRight)
            {
                targetVelocity.X++;
                newlyRight = true;
                if (targetVelocity.X >= maxTargetVelocity)
                    targetVelocity.X = maxTargetVelocity;

            }


            if (cameraState == CameraState.isGoingLeft)
            {
                targetVelocity.X--;
                newlyLeft = true;
                if (targetVelocity.X <= minTargetVelocity)
                    targetVelocity.X = minTargetVelocity;
            }

            if (cameraState == CameraState.isCompletlyRight)
            {
                if (newlyRight)
                { timerLeft = 0; timerRight = 0; newlyRight = false; }

                targetVelocity.X = 0;
                targetPosition.X = origin.X + maxTargetPosition;

            }

            if (cameraState == CameraState.isCompletlyLeft)
            {
                if (newlyLeft)
                { timerLeft = 0; timerRight = 0; newlyLeft = false; }

                targetVelocity.X = 0;
                targetPosition.X = origin.X + minTargetPosition;

            }


            if (cameraState == CameraState.isCentering)
            {
                targetVelocity.X = 0;

                if (targetPosition.X > origin.X)
                {
                    targetVelocity.X=-2.5f;
                    if (targetVelocity.X < minTargetVelocity)
                        targetVelocity.X = minTargetVelocity;

                    if (targetPosition.X <= origin.X)
                    {

                        cameraState = CameraState.isCentered;

                    }
                }
                                
                if (targetPosition.X < origin.X)
                {
                    targetVelocity.X=+2.5f;
                    if (targetVelocity.X > maxTargetVelocity)
                        targetVelocity.X = maxTargetVelocity;

                    if (targetPosition.X >= origin.X)
                    {
                        targetVelocity.X = 0;
                        cameraState = CameraState.isCentered;

                    }
                }
            }
        }
    }

}