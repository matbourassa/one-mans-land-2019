﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;


namespace One_Man_s_Land
{
    public static class SoundFx
    {

        

        public static SoundEffect EnemyType01GunShot;
        public static SoundEffect ShieldImpact;

        public static void Load(ContentManager content)
        {
           
            EnemyType01GunShot = content.Load<SoundEffect>("GunsEffect/magnum44");
            ShieldImpact = content.Load<SoundEffect>("GunsEffect/Metal impact");
           
        }


        public static void EnemyType01GunShotEffect()
        { EnemyType01GunShot.Play(); }


        public static void ShieldImpactEffect()
        { ShieldImpact.Play(0.3f, 0f, 0); }


        





    }

}
