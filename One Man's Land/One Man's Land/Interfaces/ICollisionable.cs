﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace One_Man_s_Land
{
    interface ICollisionable
    {

        Vector2 GetPosition();

        Rectangle GetCollisionRectangle();

        Type GetType();

        AnimationState GetState();

        void ReactionToCollision(Type type, Vector2 Position, AnimationState playerState);



    }
}
