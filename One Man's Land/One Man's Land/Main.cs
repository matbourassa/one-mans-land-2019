﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace One_Man_s_Land
{
    class Main  //entry point of the solution, call the gameloop and the debug console
    {
        GameCreator gameCreator;                        //builder of the game and its main objects (namely the levels and the player) - others objets will be created by the levels
        DebugConsole debugConsole;                      //showing game data for debug purpose
        GameLoop gameLoop;                              //runs the game
        
        public void Initialize(GraphicsDevice graphicsDevice, GraphicsDeviceManager graphics, ContentManager content)
        {
            gameCreator = new GameCreator(graphics, graphicsDevice);          //creates the game builder + passing the core objects from the root <Game> of xna
            gameCreator.Initialize(content);                                  //creates levels and player

            debugConsole = new DebugConsole(gameCreator);       //console has access to the whole game through the gameCreator      
            debugConsole.Initialize(graphicsDevice);

            gameLoop = new GameLoop(gameCreator);               //gameLoop has access to the whole game through the gameCreator
            gameLoop.Initialize(graphicsDevice);
        }

        public void Load(ContentManager content)      //Loading game assets
        {
            debugConsole.Load(content);
            gameLoop.Load(content);
        }
        
        public void Update(GameTime gameTime)        //looping through the game and the console
        {
            debugConsole.Update(gameTime);
            gameLoop.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)          //Drawing the game and the console
        {
            gameLoop.Draw(spriteBatch);
            debugConsole.Draw(spriteBatch);
        }
    }
}
