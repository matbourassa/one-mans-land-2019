﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace One_Man_s_Land
{
    class CollisionTest
    {


        Texture2D BackGroundTexture;
        Texture2D BallTexture;
        Texture2D RailTexture;
        Vector2 BackGroundPosition;
        Vector2 BallPosition;
        Vector2 BallVelocity;
        Vector2 PreviousBallPosition;
        Vector2 RailPosition;
        Rectangle BackGroundRectangle;
        Rectangle BallRectangle;
        Rectangle BallRectangleTop;
        Rectangle BallRectangleBottom;
        Rectangle BallRectangleRight;
        Rectangle BallRectangleLeft;
        



        enum CollisionState{HitBottom, HitTop, HitRight, HitLeft, WalkRight, WalkLeft, NoHit};
        CollisionState collisiontState = new CollisionState();


        public void Initialize()
        {
            BackGroundPosition = new Vector2(0, 0);
            RailPosition = new Vector2(0, 610);
            BackGroundRectangle = new Rectangle(0, 660, 1680, 250);
            BallPosition = new Vector2(700, 200);
            PreviousBallPosition = new Vector2(800, 500);
            BallVelocity = new Vector2(0, 0);
            collisiontState = CollisionState.NoHit;
            
            
        }



        public void Load(ContentManager content)
        {

            BackGroundTexture = content.Load<Texture2D>("Collision Test/Background test 7");
            BallTexture = content.Load<Texture2D>("Collision Test/playertest1");
            RailTexture = content.Load<Texture2D>("Collision Test/BackgroundRail 7");
        }



        public void Update(GameTime gameTime)
        {
            
            BallPosition += BallVelocity;
            
            BallRectangle = new Rectangle((int)BallPosition.X, (int)BallPosition.Y, BallTexture.Width, BallTexture.Height);
            
            
            BallRectangleTop = new Rectangle((int)BallPosition.X, (int)BallPosition.Y, 40, 20);
            BallRectangleBottom = new Rectangle((int)BallPosition.X +34, (int)BallPosition.Y + 256, 20, 20); //Zone de détection de contact avec le sol
            
            BallRectangleRight = new Rectangle((int)BallPosition.X + 30, (int)BallPosition.Y + 20, 10, 60);
            BallRectangleLeft = new Rectangle((int)BallPosition.X, (int)BallPosition.Y + 20 , 10, 60);


            
            

            
            
            
            if (BallRectangleBottom.Intersects(BackGroundRectangle))
            {
                PixelCollision(RailTexture, BallTexture, BackGroundRectangle, BallRectangle);
                //collisiontState = CollisionState.HitBottom;
            }
            
            
            Controller();

       }



        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            //spriteBatch.Draw(BackGroundTexture, BackGroundPosition, Color.White);
            spriteBatch.Draw(RailTexture, RailPosition, Color.White);
            spriteBatch.Draw(BallTexture, BallRectangle, Color.White);
            spriteBatch.End();

        }


        public bool PixelCollision(Texture2D BackGround, Texture2D Ball, Rectangle BackGroundRec, Rectangle BallRec)
        {
            Color[] ColorData1 = new Color[BackGround.Width * BackGround.Height];
            Color[] ColorData2 = new Color[Ball.Width * Ball.Height];

            BackGround.GetData<Color>(ColorData1);
            Ball.GetData<Color>(ColorData2);

            int top = Math.Max(BackGroundRec.Top, BallRec.Top);
            int bottom = Math.Min(BackGroundRec.Bottom, BallRec.Bottom);
            int left = Math.Max(BackGroundRec.Left, BallRec.Left);
            int right = Math.Min(BackGroundRec.Right, BallRec.Right);

            for (int y = top; y < bottom; y++)
            {
                for (int x = left; x < right; x++)
                {
                    Color A = ColorData1[(y - BackGroundRec.Top) * (BackGroundRec.Width) + (x - BackGroundRec.Left)];
                    Color B = ColorData2[(y - BallRec.Top) * (BallRec.Width) + (x - BallRec.Left)];

                    if (A.A != 0 && B.A != 0)
                    {
                        BallPosition.Y = (y - 256);  //Point de contact avec le plancher, moins la hauteur du sprite 
                        return true;
                    }


                }

            }
            return false;
        }


        public void Controller()
        {

            KeyboardState keystate = Keyboard.GetState();
            GamePadState gamePadState = GamePad.GetState(PlayerIndex.One);
            BallVelocity.X = 0;
            
            BallVelocity.Y = 12f;  //Gravity

            if ((keystate.IsKeyDown(Keys.Left)) || (gamePadState.DPad.Left == ButtonState.Pressed))
            {
                BallVelocity.X = -10;
                
            }


            if ((keystate.IsKeyDown(Keys.Right)) || (gamePadState.DPad.Right == ButtonState.Pressed))
            {
                BallVelocity.X = 10;
                
            }


            if ((keystate.IsKeyDown(Keys.Space)) || (gamePadState.Buttons.A == ButtonState.Pressed))
            {
                BallVelocity.Y = -5;
                BallPosition.Y -= 5;
            }

           


        }

    }
}
