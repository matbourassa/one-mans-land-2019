﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;

namespace One_Man_s_Land
{
    public class Shield
    {

        Texture2D ShieldSpreadSheet;
        Vector2 ShieldPosition = new Vector2();
        Rectangle FrameToDraw;
        int CurrentFrameX;
        int CurrentFrameY;
        const int ShieldHeight = 296;
        const int ShieldWidth = 232;
        float AnimationTimer;
        float AnimationInterval = 30; 
        float ShieldOnAnimationInterval = 100;


        Texture2D HitTexture;
        Vector2 HitPosition = new Vector2();
        public int HitTimer;
        AnimationHelper bulletHitAnimationHelper = new AnimationHelper(25, 25);
        public bool Sound;

        AnimationShieldState animationShieldState = new AnimationShieldState();
        public CollisionShieldState collisionShieldState = new CollisionShieldState();

        public bool ShieldBool = false;
        bool ShieldGrown = false;
                
        public Shield()
        {  }


        public void Load(ContentManager content)
        {

            animationShieldState = AnimationShieldState.ShieldOff;
            collisionShieldState = CollisionShieldState.hasNotBeenHit;
            ShieldSpreadSheet = content.Load<Texture2D>("Shield/SpreadSheetShield");
            HitTexture = content.Load<Texture2D>("Shield/BulletHitSpreadSheet");
            bulletHitAnimationHelper.Load(HitTexture);


        }


        public void ShieldController(GamePadState gamePadState, GamePadState oldPadState)
        {
            
            if ((gamePadState.Triggers.Right != 0) && (oldPadState.Triggers.Right != 0))
            {

                ShieldBool = true;
                if (!ShieldGrown)
                    animationShieldState = AnimationShieldState.ShieldIn; //Le Shield en train de grandir
                if (ShieldGrown)
                    animationShieldState = AnimationShieldState.ShieldOn; //Le shield complètement formé
            }


            if (gamePadState.Triggers.Right == 0)
            {

                ShieldGrown = false;
                animationShieldState = AnimationShieldState.ShieldOut;

            }

        
        }


        public void Update(GameTime gameTime, Vector2 PlayerPosition, string LastStanding)
        {

                   
            FrameToDraw = new Rectangle(CurrentFrameX * ShieldWidth, CurrentFrameY * ShieldHeight, ShieldWidth, ShieldHeight);
            
            if(LastStanding == "Right")
                ShieldPosition.X = PlayerPosition.X;

            if (LastStanding == "Left")
                ShieldPosition.X = PlayerPosition.X - 40;

            ShieldPosition.Y = PlayerPosition.Y - 30;

            if (ShieldBool)
            {
                if ((animationShieldState == AnimationShieldState.ShieldIn))
                {
                    AnimationTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                    CurrentFrameY = 0;

                    if (AnimationTimer > AnimationInterval)
                    {
                        CurrentFrameX++;
                        AnimationTimer = 0;
                    }
                    if (CurrentFrameX >= 9)
                        ShieldGrown = true;
                }

                if ((animationShieldState == AnimationShieldState.ShieldOut))
                {
                    AnimationTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                    CurrentFrameY = 0;

                    if (AnimationTimer > AnimationInterval)
                    {
                        CurrentFrameX--;
                        AnimationTimer = 0;
                    }
                    if (CurrentFrameX <= 0)
                    {
                        animationShieldState = AnimationShieldState.ShieldOut;
                        ShieldBool = false;
                    }

                }

                if (animationShieldState == AnimationShieldState.ShieldOn)
                {

                    AnimationTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                    CurrentFrameY = 2;

                    if (AnimationTimer > ShieldOnAnimationInterval)
                    {
                        CurrentFrameX++;
                        AnimationTimer = 0;
                    }
                    if (CurrentFrameX > 8)
                        CurrentFrameX = 0;

                
                }
                                

            }
        }
        
        public void Draw(SpriteBatch spriteBatch)
        {
            if (ShieldBool)
            spriteBatch.Draw(ShieldSpreadSheet, ShieldPosition, FrameToDraw, Color.White);
            
        }


        public bool UpdateHit(GameTime gameTime, float PlayerPositionX, float PlayerPositionY)
        {
            if (Sound)
            { SoundFx.ShieldImpactEffect(); Sound = false; }

            HitPosition.X = PlayerPositionX;
            HitPosition.Y = PlayerPositionY;

            HitTimer--;
            bulletHitAnimationHelper.Update(gameTime, 100, 0, 8, false);
            if (HitTimer < 0)
            return false;

            return true;
        }


        public void DrawHit(SpriteBatch spriteBatch)
        {
            if (HitTimer > 0)
            bulletHitAnimationHelper.Draw(spriteBatch, HitPosition);
        }



    }
}
