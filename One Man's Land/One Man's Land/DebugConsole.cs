﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace One_Man_s_Land
{
    class DebugConsole
    {
        GameCreator gameCreator;       //The debug console access the game through the gameCreator object, passed by the Main

        List<ICollisionable> ToDisplay = new List<ICollisionable>();  //List of game objets to display on the console
        List<Vector2> TextPosisionList = new List<Vector2>();         //List of locations for the console text

        KeyboardState oldState;        //To toggle the console on/off
        SpriteFont spriteFontStandard;  //Text font for the console

        int currentTextPosition = 0;   //Initialize the first location of the text
        bool DebugerOn = true;         //To keep track of the on/off state of the console

        public DebugConsole(GameCreator gameCreator)
        {
            this.gameCreator = gameCreator;    //passing the current game to the console
        }

        public void Initialize(GraphicsDevice graphicsDevice)
        {
            //Create the different locations for the text and store it into TextPositionList
            Vector2 TextPosition1 = new Vector2(0);
            Vector2 TextPosition2 = new Vector2(0, 40);
            Vector2 TextPosition3 = new Vector2(0, 60);
            Vector2 TextPosition4 = new Vector2(0, 80);
            Vector2 TextPosition5 = new Vector2(0, 120);
            Vector2 TextPosition6 = new Vector2(0, 140);
            Vector2 TextPosition7 = new Vector2(0, 160);
            Vector2 TextPosition8 = new Vector2(0, 200);
            Vector2 TextPosition9 = new Vector2(0, 220);
            Vector2 TextPosition10 = new Vector2(0, 240);
            Vector2 TextPosition11 = new Vector2(0, 280);
            Vector2 TextPosition12 = new Vector2(0, 300);
            Vector2 TextPosition13 = new Vector2(0, 320);
            Vector2 TextPosition14 = new Vector2(0, 360);
            Vector2 TextPosition15 = new Vector2(0, 380);
            Vector2 TextPosition16 = new Vector2(0, 400);

            TextPosisionList.Add(TextPosition1);
            TextPosisionList.Add(TextPosition2);
            TextPosisionList.Add(TextPosition3);
            TextPosisionList.Add(TextPosition4);
            TextPosisionList.Add(TextPosition5);
            TextPosisionList.Add(TextPosition6);
            TextPosisionList.Add(TextPosition7);
            TextPosisionList.Add(TextPosition8);
            TextPosisionList.Add(TextPosition9);
            TextPosisionList.Add(TextPosition10);
            TextPosisionList.Add(TextPosition11);
            TextPosisionList.Add(TextPosition12);
            TextPosisionList.Add(TextPosition13);
            TextPosisionList.Add(TextPosition14);
            TextPosisionList.Add(TextPosition15);
            TextPosisionList.Add(TextPosition16);
        }

        public void Load(ContentManager content)
        {
            spriteFontStandard = content.Load<SpriteFont>("Font/SpriteFont1");
        }

        public void Update(GameTime gameTime)  //refreshed at each cycle
        {
            KeyboardState keyState = Keyboard.GetState();

            if ((keyState.IsKeyDown(Keys.OemTilde)) && (oldState.IsKeyUp(Keys.OemTilde)))  //Toggling the console on/off by pressing the tilde key
            { DebugerOn = !DebugerOn; }
            oldState = keyState;  //keeping track of the previous cycle keystate to avoid trigerring an on/off flickering

            ToDisplay = gameCreator.levelTest1.collisionManagerLevelTest.collisionableObjectList; //copying the collisionable list from the game here at each cycle to
                                                                                                  //display a dynamic and comprehensive list of current game objects

        }

        public void Draw(SpriteBatch spriteBatch) //refreshed at each cycle
        {
            if (DebugerOn)
            {
                spriteBatch.Begin();

                //writing the number of element in the current ToDiplay list
                spriteBatch.DrawString(spriteFontStandard, "Items in collision manager = " + ToDisplay.Count(), TextPosisionList[currentTextPosition], Color.DarkRed);

                //writing three different available variables for every single item in the ToDisplay list
                foreach (var item in ToDisplay)
                {
                    currentTextPosition++;
                    spriteBatch.DrawString(spriteFontStandard, item.GetType() + " Position = " + item.GetPosition(), TextPosisionList[currentTextPosition], Color.Black); //GetType gives and writes the name of the game object down
                    currentTextPosition++;
                    spriteBatch.DrawString(spriteFontStandard, item.GetType() + " State = " + item.GetState(), TextPosisionList[currentTextPosition], Color.Black);
                    currentTextPosition++;
                    spriteBatch.DrawString(spriteFontStandard, item.GetType() + " Rectangle = " + item.GetCollisionRectangle(), TextPosisionList[currentTextPosition], Color.Black);
                }
                currentTextPosition = 0;  //once the loop ends for a cycle, we start anew

                spriteBatch.End();
            }
        }
    }
}
