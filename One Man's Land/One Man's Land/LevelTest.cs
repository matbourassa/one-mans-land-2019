﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace One_Man_s_Land
{
    class LevelTest
    {

        Player player;
        EnemyType1 enemyType1;
        HealthBar healthBar;
                
        CameraDynamic camera;
        CameraDynamic cameraMiddle;
        CameraDynamic cameraFar;
        Fog fog;
        
        public CollisionManager collisionManagerLevelTest;
                             
        Texture2D FocalGround01Texture;
        Texture2D MiddleGround01Texture;
        Texture2D FarGround01Texture;
        Texture2D SkyTexture;

        Vector2 FocalGroundPosition;
        Vector2 MiddleGroundPosition;
        Vector2 FarGroundPosition;
        Vector2 SkyPosition;

        
        public void Initialize(GraphicsDevice graphicsDevice)
        {
            collisionManagerLevelTest = new CollisionManager();

            player = new Player(collisionManagerLevelTest);
            enemyType1 = new EnemyType1(player, collisionManagerLevelTest); 
                    
            camera = new CameraDynamic(graphicsDevice.Viewport, player);
            cameraMiddle = new CameraDynamic(graphicsDevice.Viewport, player);
            cameraFar = new CameraDynamic(graphicsDevice.Viewport, player);
            fog = new Fog();

            

            healthBar = new HealthBar();
            

            
            
            enemyType1.Initialize(graphicsDevice);
            player.Initialize();
            healthBar.Initialize();
            
            FocalGroundPosition = new Vector2(0);
            MiddleGroundPosition = new Vector2(0, -150);
            FarGroundPosition = new Vector2(0, -100);
            SkyPosition = new Vector2(0);

        }

        public void Load(ContentManager content)
        {
            player.Load(content);
            enemyType1.Load(content);

            collisionManagerLevelTest.AddCollisionableObject(player);
            collisionManagerLevelTest.AddCollisionableObject(enemyType1);
            

            healthBar.Load(content);
            fog.Load(content);
            
                        
            FocalGround01Texture = content.Load<Texture2D>("BackGrounds/Level01FocalGround01");
            MiddleGround01Texture = content.Load<Texture2D>("Backgrounds/Level01MiddleGround01");
            FarGround01Texture = content.Load<Texture2D>("BackGrounds/Level01FarGround01");
            SkyTexture = content.Load<Texture2D>("BackGrounds/Level01Sky01");
            
            camera.Load();
            cameraMiddle.Load();
            cameraFar.Load();
         
        }
        
        public void Update(GameTime gameTime)
        {
            
            player.Update(gameTime, FocalGround01Texture);
            fog.Update();
            enemyType1.Update(gameTime, FocalGround01Texture);
            healthBar.Update();
            

            collisionManagerLevelTest.LoopCollisionDetection();

            camera.Update(player.PlayerPosition, FocalGround01Texture.Width, FocalGround01Texture.Height, 1, gameTime);
            cameraMiddle.Update(player.PlayerPosition, MiddleGround01Texture.Width, MiddleGround01Texture.Height, 0.25f, gameTime);
            cameraFar.Update(player.PlayerPosition, FarGround01Texture.Width, FarGround01Texture.Height, 0.15f, gameTime);
        }
        
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(SkyTexture, SkyPosition, Color.White);
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, cameraFar.Transform);
            spriteBatch.Draw(FarGround01Texture, FarGroundPosition, Color.White);
            spriteBatch.End();
            
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, cameraMiddle.Transform);
            spriteBatch.Draw(MiddleGround01Texture, MiddleGroundPosition, Color.White);
            fog.DrawBackFog(spriteBatch);
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, camera.Transform);
            spriteBatch.Draw(FocalGround01Texture, FocalGroundPosition, Color.White);
            enemyType1.Draw(spriteBatch);
            player.Draw(spriteBatch);
            fog.DrawFrontFog(spriteBatch);
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Additive, null, null, null, null, camera.Transform);
            player.shield.Draw(spriteBatch);
            spriteBatch.End();

            spriteBatch.Begin();
            healthBar.Draw(spriteBatch);
            spriteBatch.End();

        }
    }
}
